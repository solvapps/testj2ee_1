package tests;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import repositories.EmployeeRepository;

@ManagedBean(name = "helloWorldBean")
//@Named
@SessionScoped
public class HelloWorldBean {
	@Inject
	private EmployeeRepository employeerepo;
	public String getMsg() {
		return "Hallo";
	}
	public String getEmployees() {
		return String.valueOf(employeerepo.getEmployees().size());
	}
}
