package repositories;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import javax.ejb.Stateless;
import javax.inject.Named;

import entities.Employee;

@Stateless
@Named
public class EmployeeRepository {
	@PersistenceContext
	EntityManager entityManager;
	public EmployeeRepository() {
		
	}
	public List<Employee> getEmployees(){
		TypedQuery<Employee> qu = entityManager.createQuery("select * from Employee", Employee.class);
		List<Employee> emp2 = qu.getResultList();
		return emp2;
	}
}
